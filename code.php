<?php

class Building{
	
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
};

class Bldg extends Building{

	//getter
	public function getName(){
		$this->name;
			return "The name of the Building is $this->name";
	}
	public function getFloors(){
		$this->name;
		$this->floors;
			return "The $this->name has $this->floors floors.";
	}
	public function getAddress(){
		$this->name;
		$this->address;
			return "The $this->name is located at $this->address";
	}
};

class Complex extends Building{
	//getter
	public function getName(){
		return $this->name;
	}

	//setter
	public function setName($name){
		$this->name = $name;
	}
};


class Condo extends Building{

	//getter
	public function getName(){
		$this->name;
			return "The name of the Condominium is $this->name";
	}
	public function getFloors(){
		$this->name;
		$this->floors;
			return "The $this->name has $this->floors floors.";
	}
	public function getAddress(){
		$this->name;
		$this->address;
			return "The $this->name is located at $this->address";
	}
};

class Tower extends Building{
	//getter
	public function getName(){
		return $this->name;
	}

	//setter
	public function setName($name){
		$this->name = $name;
	}
};


$bldg = new Bldg("Caswynn Building", 8, "Timog Ave. Quezon City, Philippines");
$complex = new Complex("Caswynn Building", 8, "Timog Ave. Quezon City, Philippines");

$condo = new Condo("Enzo Condo", 5, "Buendia Ave. Quezon City, Philippines");
$tower = new Tower("Enzo Condo", 5, "Buendia Ave. Quezon City, Philippines");