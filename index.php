<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S4: Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>Building</h1>
	<p><?= $bldg->getName(); ?></p>
	<p><?= $bldg->getFloors(); ?></p>
	<p><?= $bldg->getAddress(); ?></p>
	<?php $complex->setName('Enzo Complex') ; ?>
	<p>The name of the Building has been changed to <?= $complex->getName(); ?></p>

	<h1>Condominium</h1>
	<p><?= $condo->getName(); ?></p>
	<p><?= $condo->getFloors(); ?></p>
	<p><?= $condo->getAddress(); ?></p>

	<?php $tower->setName('Enzo Tower') ; ?>
	<p>The name of the Condominium has been changed to <?= $tower->getName(); ?></p>

</body>
</html>
